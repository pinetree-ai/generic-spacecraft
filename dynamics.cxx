#include "Dynamix.hxx"

#define PI 3.14159265358979323846
#define GAMMA_LEN 6*3 // 6DOF * 3 bodies

// This project is an example of a system of multiple rigid bodies that interact with gravitational fields

// This project is also a good example of how the RigidBody class is used
// See https://aptus.aero/pinetree/reference/dynamix/index.php?p=C%2B%2B/Dynamics/RigidBody


// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: Xdot [gammad, gammadd]
// Inputs: X [gamma, gammad]
//		   U (control inputs)
//		   params (loaded parameters) - access with params["<param name>"]

vec Xdot(GAMMA_LEN * 2);

vec dynamics_cxx(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables)
{
	return Xdot;
}
