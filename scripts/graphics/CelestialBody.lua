-- Celestial body graphics class
CelestialBody = {
	-- Body name - properties are at /dynamics/gamma/<name>_<DOF>
	name = "",
	
	-- Model ID
	model = -1,
	
	-- Constructor
	new = function( name, path )
		
		o = {}
		setmetatable(o, self)
		self.__index = self
		self.name = name
		
		-- Load model
		self.model = gfx_load3d(path)
		
		return o
	
	end,
	
	-- Update model
	update = function( self )
	
		-- Load properties
		local x = getprop( "/dynamics/gamma/" .. self.name .. "_x" )
		local y = getprop( "/dynamics/gamma/" .. self.name .. "_y" )
		local z = getprop( "/dynamics/gamma/" .. self.name .. "_z" )
		--local phi = getprop( "/dynamics/gamma/" .. self.name .. "_phi" )
		--local theta = getprop( "/dynamics/gamma/" .. self.name .. "_theta" )
		--local psi = getprop( "/dynamics/gamma/" .. self.name .. "_psi" )
	
		-- Translation
		gfx_setTrans( self.model, x, y, z )
		
		-- Rotation
		--gfx_setEuler( self.model, phi, theta, psi )
		
	end
}
