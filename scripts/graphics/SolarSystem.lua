if init then

	-- Load CelestialBody class library

	-- Initialize Celestial bodies in the Solar system
	ss = {
		
		-- Sun
		CelestialBody:new( "Sun", "model/GenericSpacecraft/3d/planets/Sun.ac" ),
		
		-- Earth
		CelestialBody:new( "Earth", "model/GenericSpacecraft/3d/planets/Earth.ac" )
	
	}

end

-- Update all bodies in the Solar system
for i, body in ipairs(ss) do
	print( body:name )
end
